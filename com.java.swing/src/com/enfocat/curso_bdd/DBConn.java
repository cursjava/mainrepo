package com.enfocat.curso_bdd;


import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;

import java.util.*;

public class DBConn {

	private static final String URL = "jdbc:mysql://192.168.10.10:3306/curso";
	private static final String USERNAME = "ricard";
	private static final String PASSWORD = "1q2w3e4r";
	private static final Logger LOG = Logger.getLogger(DBConn.class.getName());

	
	public static Alumno leeAlumno (int id_alumno) { 

		
		
		LOG.info("INICIO carga alumno");
		Alumno alumno = null;
		
		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select * from alumnos where alumno_id="+id_alumno);
			
			rs.next();

			alumno = new Alumno(
						(Integer) rs.getObject(1),
						(String) rs.getObject(2),
						(String) rs.getObject(3),
						(String) rs.getObject(4),
						(String) rs.getObject(5)
						);
			

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando alumno", e);
		}

		LOG.info("INICIO carga alumno");
		
		return alumno;

	}
	
	public static List<Alumno> leeAlumnos() {

		LOG.info("INICIO carga alumnos");
		List<Alumno> alumnoList = new ArrayList<Alumno>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select alumno_id,nombre,telefono,email,poblacion from alumnos");

			while (rs.next()) {
				alumnoList.add(new Alumno(
						(Integer) rs.getObject(1),
						(String) rs.getObject(2),
						(String) rs.getObject(3),
						(String) rs.getObject(4),
						(String) rs.getObject(5)
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando alumnos", e);
		}

		LOG.info("INICIO carga alumnos");
		return alumnoList;

	}
	
	

	public static List<Examen> leeExamenes() {

		LOG.info("INICIO carga examenes");
		List<Examen> examenList = new ArrayList<Examen>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select examen_id,nombre from examenes");

			while (rs.next()) {
				examenList.add(new Examen(
						(Integer) rs.getObject(1),
						(String) rs.getObject(2)
			
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando examenes", e);
		}

		LOG.info("INICIO carga examenes");
		return examenList;

	}
	
	public static void creaAlumno(Alumno al) {

		LOG.info("INICIO crea alumno");
	
		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			StringBuilder sb = new StringBuilder();
			
			sb.append("INSERT INTO alumnos ");
			sb.append("(nombre, telefono, email, poblacion)");
			sb.append(" VALUES (");
			sb.append("\"" + al.getNombre() +"\", ");
			sb.append("\"" + al.getTelefono() +"\", ");
			sb.append("\"" + al.getEmail() +"\", ");
			sb.append("\"" + al.getPoblacion() +"\") ");
			
				
			stmt.executeUpdate(sb.toString());

			

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error crea alumno", e);
		}

		LOG.info("FIN crea alumno");


	}
	
	
	public static void modificaAlumno(Alumno al) {

		LOG.info("INICIO modifica alumno");
	
		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			StringBuilder sb = new StringBuilder();
			
			sb.append("UPDATE alumnos SET ");
			sb.append("nombre = \"" + al.getNombre() +"\", ");
			sb.append("telefono = \"" + al.getTelefono() +"\", ");
			sb.append("email = \"" + al.getEmail() +"\", ");
			sb.append("poblacion = \"" + al.getPoblacion() +"\" ");
			sb.append("WHERE alumno_id=" + al.getAlumno_id());
			
			String query = sb.toString();
			
			LOG.info(query);
			stmt.executeUpdate(query);

			

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error modifica alumno", e);
		}

		LOG.info("FIN modifica alumno");


	}
	

	public static List<Ciudad> leeCiudades() {

		LOG.info("INICIO carga ciudades");
		List<Ciudad> ciudadList = new ArrayList<Ciudad>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select distinct poblacion from alumnos");

			while (rs.next()) {
				ciudadList.add(new Ciudad(
						(String) rs.getObject(1)
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando ciudades", e);
		}

		LOG.info("INICIO carga ciudades");
		return ciudadList;

	}
	

	public static List<Alumno> leeAlumnosCiudad(String ciudad) {

		LOG.info("INICIO carga alumnos ciudad");
		List<Alumno> alumnoList = new ArrayList<Alumno>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select alumno_id,nombre,telefono,email,poblacion from alumnos where poblacion=\""+ciudad+"\"");

			while (rs.next()) {
				alumnoList.add(new Alumno(
						(Integer) rs.getObject(1),
						(String) rs.getObject(2),
						(String) rs.getObject(3),
						(String) rs.getObject(4),
						(String) rs.getObject(5)
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Error cargando alumnos ciudad", e);
		}

		LOG.info("INICIO carga alumnos ciudad");
		return alumnoList;

	}
	
	
}
