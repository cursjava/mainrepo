package com.enfocat.curso_bdd;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.FormSpecs;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import com.jgoodies.forms.layout.Sizes;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NuevoAlumno extends JInternalFrame {
	private JTextField textFieldNombre;
	private JTextField textFieldEmail;
	private JTextField textFieldTelefono;
	private JTextField textFieldPoblacion;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NuevoAlumno frame = new NuevoAlumno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NuevoAlumno() {
		setClosable(true);
		setTitle("Nuevo Alumno");
		setBounds(100, 100, 465, 208);
		getContentPane().setLayout(new MigLayout("", "[10px:n][61px][355px][10px:n]", "[26px][26px][26px][26px][29px]"));
		
		JLabel lblNombre = new JLabel("Nombre");
		getContentPane().add(lblNombre, "cell 1 0,alignx right,aligny center");
		
		textFieldNombre = new JTextField();
		getContentPane().add(textFieldNombre, "cell 2 0,growx,aligny top");
		textFieldNombre.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		getContentPane().add(lblEmail, "cell 1 1,alignx right,aligny center");
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		getContentPane().add(textFieldEmail, "cell 2 1,growx,aligny top");
		
		JLabel lblTelfono = new JLabel("Teléfono");
		getContentPane().add(lblTelfono, "cell 1 2,alignx right,aligny center");
		
		textFieldTelefono = new JTextField();
		textFieldTelefono.setColumns(10);
		getContentPane().add(textFieldTelefono, "cell 2 2,growx,aligny top");
		
		JLabel lblPoblacin = new JLabel("Población");
		getContentPane().add(lblPoblacin, "cell 1 3,alignx right,aligny center");
		
		textFieldPoblacion = new JTextField();
		textFieldPoblacion.setColumns(10);
		getContentPane().add(textFieldPoblacion, "cell 2 3,growx,aligny top");
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//creamos alumno
				Alumno al = new Alumno(textFieldNombre.getText(), 
						textFieldTelefono.getText(), textFieldEmail.getText(), textFieldPoblacion.getText());
				DBConn.creaAlumno(al);
				
				//ocultamos ventana
				JButton j = (JButton) e.getSource();
				SwingUtilities.getAncestorOfClass(JInternalFrame.class, j).setVisible(false);;
			
			}
		});
		getContentPane().add(btnNewButton, "cell 1 4 2 1,alignx center,aligny top");

	}

}
