package com.enfocat.curso_bdd;

public class Ciudad {
	private String nombre;

	@Override
	public String toString() {
		return "Ciudad [nombre=" + nombre + "]";
	}
	public Ciudad(String nombre) {
		super();
		this.nombre = nombre;
	}
	public Ciudad() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
