package com.enfocat.curso_bdd;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import java.awt.FlowLayout;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Escritorio extends JFrame {

	private JPanel contentPane;
	public static JDesktopPane desktopPane;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Escritorio frame = new Escritorio();
					//frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Escritorio() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 805, 559);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMen = new JMenu("Menú");
		menuBar.add(mnMen);
		
		JMenuItem mntmAlumnos = new JMenuItem("Alumnos");
		mntmAlumnos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaAlumnos vista = new VistaAlumnos();
				vista.setVisible(true);
				vista.setLocation(10, 10);
				desktopPane.add(vista);
			}
		});
		mnMen.add(mntmAlumnos);
		
		JMenuItem mntmExmenes = new JMenuItem("Exámenes");
		mnMen.add(mntmExmenes);
		
		JMenuItem mntmCiudades = new JMenuItem("Ciudades");
		mntmCiudades.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VistaCiudades vista = new VistaCiudades();
				vista.setVisible(true);
				vista.setLocation(10, 10);
				desktopPane.add(vista);
			}
		});
		mnMen.add(mntmCiudades);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
}
