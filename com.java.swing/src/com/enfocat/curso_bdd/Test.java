package com.enfocat.curso_bdd;

import java.util.ArrayList;
import java.util.List;


public class Test {

	private static List<Alumno> alumnosList = new ArrayList<Alumno>();
	
	private static List<Examen> examenesList = new ArrayList<Examen>();
	
	public static void main(String[] args) {
		
		Alumno al2 = new Alumno(0,"Indiana","622622","indiana@jones.com","xxxx");
		DBConn.creaAlumno(al2);
		
		alumnosList=DBConn.leeAlumnos();
		
		for (Alumno alumno : alumnosList){
			System.out.println(alumno.toString());
		}
		
		examenesList=DBConn.leeExamenes();
		
		for (Examen examen : examenesList){
			System.out.println(examen.toString());
		}
		
		System.out.println(">>>>");
		Alumno al;
		al=DBConn.leeAlumno(2);
		System.out.println(al.toString());
		
	
		
	}

}
