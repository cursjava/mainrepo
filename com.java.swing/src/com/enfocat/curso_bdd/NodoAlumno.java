package com.enfocat.curso_bdd;

import javax.swing.tree.DefaultMutableTreeNode;

public class NodoAlumno extends DefaultMutableTreeNode {
	private Alumno alumno;

	public NodoAlumno(Alumno al) {
		super(al.getNombre());
		this.alumno=al;
		
	}
	
	public NodoAlumno() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NodoAlumno(Object userObject, boolean allowsChildren) {
		super(userObject, allowsChildren);
		// TODO Auto-generated constructor stub
	}

	public NodoAlumno(Object userObject) {
		super(userObject);
		// TODO Auto-generated constructor stub
	}

	public Alumno getAlumno() {
		return alumno;
	}


	
	@Override
	public String toString() {
		return alumno.getNombre() ;
		
	}

	
}
