package com.enfocat.curso_bdd;

import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;


public class VistaCiudades extends JInternalFrame {
	private JTree tree;
	private JTextField textFieldEmail;

	public void fillData(){
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Ciudades");
		
		DefaultMutableTreeNode nodoCiudadActual ;
		List<Alumno> alumnos=null;
		List<Ciudad> ciudades = DBConn.leeCiudades();
		
		for (Ciudad city : ciudades) {
			nodoCiudadActual = new DefaultMutableTreeNode(city.getNombre());
			alumnos=DBConn.leeAlumnosCiudad(city.getNombre());
			for(Alumno al : alumnos){
				//ct.add(new NodoAlumno(al));
				nodoCiudadActual.add(new DefaultMutableTreeNode(al.getNombre()));
			}
			root.add(nodoCiudadActual);
		}
			
		DefaultTreeModel defaultTreeModel = new DefaultTreeModel(root);
		tree.setModel(defaultTreeModel);
	}
	
	public void fillData2(){
		List<Ciudad> ciutats = DBConn.leeCiudades();
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Cats");
		
			DefaultMutableTreeNode category1 = new DefaultMutableTreeNode("cat1");
				category1.add(new DefaultMutableTreeNode("cat1.1"));
				category1.add(new DefaultMutableTreeNode("cat1.2"));
				category1.add(new DefaultMutableTreeNode("cat1.3"));
			root.add(category1);
			
			DefaultMutableTreeNode category2 = new DefaultMutableTreeNode("cat2");
				category2.add(new DefaultMutableTreeNode("cat2.1"));
				
				DefaultMutableTreeNode categ22 = new DefaultMutableTreeNode("cat2.2");
					categ22.add(new DefaultMutableTreeNode("cat2.2.1"));
					categ22.add(new DefaultMutableTreeNode("cat2.2.2"));
					categ22.add(new DefaultMutableTreeNode("cat2.2.3"));
				category2.add(categ22);
		
		root.add(category2);
		
		DefaultTreeModel modelo = new DefaultTreeModel(root);
		tree.setModel(modelo);
	}
	
	
	
	/**
	 * Create the frame.
	 */
	public VistaCiudades() {
		addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
			}
		});
		setIconifiable(true);
		setClosable(true);
		setResizable(true);
		setTitle("Ciudades");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new MigLayout("", "[grow][grow]", "[grow][]"));
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 0 0 2 1,grow");
		
		tree = new JTree();
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				if (tree.isSelectionEmpty()) { return; }
				TreePath path = tree.getSelectionPath();
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) path.getLastPathComponent();
				Object userobject = selectedNode.getUserObject();
				
				String cosa=userobject.toString();
				/*
				if ( true || userobject instanceof NodoAlumno) {
					System.out.println(((NodoAlumno) userobject).getAlumno().getEmail()); 
				}
				*/
				textFieldEmail.setText(cosa);

			}
		});
		scrollPane.setViewportView(tree);
		
		JLabel lblNewLabel = new JLabel("Email");
		getContentPane().add(lblNewLabel, "cell 0 1,alignx trailing");
		
		textFieldEmail = new JTextField();
		getContentPane().add(textFieldEmail, "cell 1 1,growx");
		textFieldEmail.setColumns(10);
		fillData();
		
	}

}
