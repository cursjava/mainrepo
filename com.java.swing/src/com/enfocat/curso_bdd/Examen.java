package com.enfocat.curso_bdd;

public class Examen {
	private int examen_id;
	private String nombre;
	@Override
	public String toString() {
		return "Examen [nombre=" + nombre + "]";
	}
	public Examen(int examen_id, String nombre) {
		super();
		this.examen_id = examen_id;
		this.nombre = nombre;
	}
	public Examen() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getExamen_id() {
		return examen_id;
	}
	public void setExamen_id(int examen_id) {
		this.examen_id = examen_id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
	
}
