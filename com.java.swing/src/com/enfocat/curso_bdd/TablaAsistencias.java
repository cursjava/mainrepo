package com.enfocat.curso_bdd;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;


public class TablaAsistencias extends JPanel {
    private boolean DEBUG = false;

    public TablaAsistencias() {
        super(new GridLayout(1,0));

        JTable table = new JTable(new MyTableModel());
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);


        JScrollPane scrollPane = new JScrollPane(table);

 

        setUpAsistenciaColumn(table, table.getColumnModel().getColumn(1));


        add(scrollPane);
    }


    public void setUpAsistenciaColumn(JTable table, TableColumn asistenciaColumn) {
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Correcto");
        comboBox.addItem("Impuntual");
        comboBox.addItem("Falta");
        comboBox.addItem("Falta justificada");
       
        asistenciaColumn.setCellEditor(new DefaultCellEditor(comboBox));

        DefaultTableCellRenderer renderer =  new DefaultTableCellRenderer();
        renderer.setToolTipText("Selecciona valor");
        asistenciaColumn.setCellRenderer(renderer);
    }

    class MyTableModel extends AbstractTableModel {
        private String[] columnNames = {"Nombre", "Asistencia"};
        private Object[][] data = {
	    {"Alex",  "Impuntual"},
	    {"Ricard",  "Correcto"},
	    {"Martí",  "Falta"},
	    {"Eric",  "Falta justificada"} };


        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable necesita este método para determinar el presentador/editor para cada celda!
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        
        public boolean isCellEditable(int row, int col) {
            if (col < 1) {
                return false;
            } else {
                return true;
            }
        }

      
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of "
                                   + value.getClass() + ")");
            }

            data[row][col] = value;
            fireTableCellUpdated(row, col);

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData() {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

 
    private static void createAndShowGUI() {
        JFrame frame = new JFrame("Tabla asistencia");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        TablaAsistencias newContentPane = new TablaAsistencias();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);

        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) {
         javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
