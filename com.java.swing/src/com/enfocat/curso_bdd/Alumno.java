package com.enfocat.curso_bdd;

public class Alumno {

	private int alumno_id;
	private String nombre;
	private String telefono;
	private String email;
	private String poblacion;
	
	
	public Alumno(int alumno_id, String nombre, String telefono, String email, String poblacion) {
		super();
		this.alumno_id = alumno_id;
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
		this.poblacion = poblacion;
	}

	
	
	public Alumno( String nombre, String telefono, String email, String poblacion) {
		super();
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
		this.poblacion = poblacion;
	}
	
	public Alumno() {
	}

	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + ", email=" + email + "]";
	}

	/* GETTERS y SETTERS */
	
	public long getAlumno_id() {
		return alumno_id;
	}

	public void setAlumno_id(int alumno_id) {
		this.alumno_id = alumno_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
	
	
	
}
