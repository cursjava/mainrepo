package com.enfocat.curso_bdd;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.List;

import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import com.enfocat.codeEntities.Script;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class VistaAlumnos extends JInternalFrame {
	private JTable table;
	private List<Alumno> alumnosList=null;

	public void fillData(){
		alumnosList=DBConn.leeAlumnos();
		DefaultTableModel defaultTableModel = new DefaultTableModel();
		defaultTableModel.addColumn("Nombre");
		defaultTableModel.addColumn("Email");
		defaultTableModel.addColumn("X");
		for (Alumno al : alumnosList){
			defaultTableModel.addRow(new Object[] {al.getNombre(), al.getEmail(), true});
		}
		table.setModel(defaultTableModel);
	}
	

	/**
	 * Create the frame.
	 */
	public VistaAlumnos() {
		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new MigLayout("", "[grow][grow]", "[grow][]"));
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 0 0 2 1,grow");
		
		table = new JTable();
	
		
		
		scrollPane.setViewportView(table);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int currentRow=table.getSelectedRow();
				Alumno loAlumne = alumnosList.get(currentRow);
				ModificaAlumno mod = new ModificaAlumno(loAlumne);
				mod.setVisible(true);
			
				//ubiquem al centre
				Dimension desktopSize = Escritorio.desktopPane.getSize();
				Dimension vistaSize = mod.getSize();
				mod.setLocation((desktopSize.width - vistaSize.width)/2,
					    (desktopSize.height- vistaSize.height)/2);
				Escritorio.desktopPane.add(mod);
				mod.moveToFront();
				
			}
		});
		
		
		
		JButton btnActualiza = new JButton("Actualiza");
		btnActualiza.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				fillData();
			}
		});
		getContentPane().add(btnActualiza, "flowx,cell 0 1,alignx left");
		
		JButton btnNewButton = new JButton("Añadir alumno");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				NuevoAlumno vista = new NuevoAlumno();
				vista.setVisible(true);
				
				//ubiquem al centre
				Dimension desktopSize = Escritorio.desktopPane.getSize();
				Dimension vistaSize = vista.getSize();
				vista.setLocation((desktopSize.width - vistaSize.width)/2,
					    (desktopSize.height- vistaSize.height)/2);
				Escritorio.desktopPane.add(vista);
				vista.moveToFront();
			
			}
		});
		getContentPane().add(btnNewButton, "cell 1 1,alignx right");
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("N");
		getContentPane().add(chckbxNewCheckBox, "cell 0 1");
		
		fillData();

	}

}
