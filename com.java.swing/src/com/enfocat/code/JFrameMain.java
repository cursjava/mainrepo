package com.enfocat.code;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;



public class JFrameMain extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPaneMain;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrameMain frame = new JFrameMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrameMain() {
		setTitle("Code Poetry");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 504, 438);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnCode = new JMenu("Code");
		menuBar.add(mnCode);
		
		JMenuItem mntmScripts = new JMenuItem("Scripts");
		mntmScripts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrameScripts f = new JInternalFrameScripts();
				f.setVisible(true);
				desktopPaneMain.add(f);
			}
		});
		
		JMenuItem mntmCategories = new JMenuItem("Categories");
		mntmCategories.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrameCats f = new JInternalFrameCats();
				f.setVisible(true);
				desktopPaneMain.add(f);
			}
		});
		mnCode.add(mntmCategories);
		mnCode.add(mntmScripts);
		
		JMenuItem mntmTags = new JMenuItem("Scripts x Category");
		mntmTags.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrameCategory f = new JInternalFrameCategory();
				f.setVisible(true);
				desktopPaneMain.add(f);
			}
		});
		mnCode.add(mntmTags);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPaneMain = new JDesktopPane();
		contentPane.add(desktopPaneMain, BorderLayout.CENTER);
	}

}
