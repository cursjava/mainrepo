package com.enfocat.code;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import net.miginfocom.swing.MigLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.enfocat.DBConn.DBConn;
import com.enfocat.codeEntities.Category;
import com.enfocat.codeEntities.Script;
import javax.swing.JTextArea;
import javax.swing.JPanel;

public class JInternalFrameCategory extends JInternalFrame {
	private JTable tableScripts;
	private JComboBox comboBox;
	private List<Category> theList = new ArrayList<Category>();
	private List<Script> theListS = new ArrayList<Script>();
	private JTextArea textAreaDesc;
	private JTextArea textAreaCode;
	
	
	
	
	
	public void fillTable(int i){
		theListS=DBConn.loadScriptsCat(i);
		DefaultTableModel defaultTableModel = new DefaultTableModel();
		defaultTableModel.addColumn("Name");
		//defaultTableModel.addColumn("Desc");
		for (Script scr : theListS){
//			defaultTableModel.addRow(new Object[] {scr.getName(), scr.getDesc()});
			defaultTableModel.addRow(new Object[] {scr.getName()});
		}
		tableScripts.setModel(defaultTableModel);
	}
	
	
	private void fillCombo(){
		theList=DBConn.loadCats();
		DefaultComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<String>();
		for (Category item : theList){
			defaultComboBoxModel.addElement(item.getName());
		}
		comboBox.setModel(defaultComboBoxModel);
	}
	

	/**
	 * Create the frame.
	 */
	public JInternalFrameCategory() {
		setResizable(true);
		setClosable(true);
		setIconifiable(true);
		setMaximizable(true);
		setTitle("Tags");
		setBounds(100, 100, 577, 615);
		getContentPane().setLayout(new MigLayout("", "[150px][grow]", "[grow][150px,grow][150px,grow]"));
		
		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//hem de carregar les dades 
				int i = comboBox.getSelectedIndex();
				fillTable(i);
			}
		});
		getContentPane().add(comboBox, "cell 0 0 2 1,grow");
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 0 1 1 2,grow");
		
		tableScripts = new JTable();
		scrollPane.setViewportView(tableScripts);
		tableScripts.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int currentRow=tableScripts.getSelectedRow();
				textAreaDesc.setText(theListS.get(currentRow).getDesc());
				textAreaCode.setText(theListS.get(currentRow).getText());
			}
		});
		JPanel panelDesc = new JPanel();
		getContentPane().add(panelDesc, "cell 1 1,grow");
		panelDesc.setLayout(new MigLayout("", "[grow,grow]", "[grow,grow]"));
		
		textAreaDesc = new JTextArea();
		textAreaDesc.setLineWrap(true);
		textAreaDesc.setWrapStyleWord(true);
		textAreaDesc.setEditable(false);
		panelDesc.add(textAreaDesc, "cell 0 0,grow");
		
		JPanel panelCode = new JPanel();
		getContentPane().add(panelCode, "cell 1 2,grow");
		panelCode.setLayout(new MigLayout("", "[grow,grow]", "[grow,grow]"));
		
		textAreaCode = new JTextArea();
		textAreaCode.setWrapStyleWord(true);
		textAreaCode.setLineWrap(true);
		textAreaCode.setEditable(false);
		panelCode.add(textAreaCode, "cell 0 0,grow");
		

		fillCombo();
	}

}
