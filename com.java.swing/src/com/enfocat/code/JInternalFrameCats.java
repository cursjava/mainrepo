package com.enfocat.code;

import java.util.*;
import com.enfocat.codeEntities.*;
import com.enfocat.DBConn.*;

import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class JInternalFrameCats extends JInternalFrame {
	
	private JTable tableCategories;
	private List<Category> theList = new ArrayList<Category>();
	
	private void fillData(){
		theList=DBConn.loadCats();
		DefaultTableModel defaultTableModel = new DefaultTableModel();
		defaultTableModel.addColumn("Id");
		defaultTableModel.addColumn("Category");
		for (Category item : theList){
			defaultTableModel.addRow(new Object[] {item.getId(), item.getName()});
		}
		tableCategories.setModel(defaultTableModel);
	}
	
	
	/**
	 * Create the frame.
	 */
	public JInternalFrameCats() {
		setMaximizable(true);
		setIconifiable(true);
		setClosable(true);
		setTitle("Categories");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new MigLayout("", "[grow]", "[grow]"));
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 0 0,grow");
		tableCategories = new JTable();
		scrollPane.setViewportView(tableCategories);
		fillData();
	}

	
}
