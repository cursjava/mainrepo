package com.enfocat.code;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.util.*;
import com.enfocat.codeEntities.*;
import com.enfocat.DBConn.*;

import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import javax.swing.JTextArea;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class JInternalFrameScripts extends JInternalFrame {
	private List<Script> theList = new ArrayList<Script>();
	private JTable tableScripts;
	private JTextArea textAreaScript;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JInternalFrameScripts frame = new JInternalFrameScripts();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void fillData(){
		theList=DBConn.loadScripts();
		DefaultTableModel defaultTableModel = new DefaultTableModel();
		defaultTableModel.addColumn("Name");
		defaultTableModel.addColumn("Desc");
		for (Script scr : theList){
			defaultTableModel.addRow(new Object[] {scr.getName(), scr.getDesc()});
		}
		tableScripts.setModel(defaultTableModel);
	}
	
	
	/**
	 * Create the frame.
	 */
	public JInternalFrameScripts() {
		setClosable(true);
		setIconifiable(true);
		setResizable(true);
		setTitle("Scripts");
		setBounds(100, 100, 576, 527);
		getContentPane().setLayout(new MigLayout("", "[grow]", "[grow][grow]"));
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane, "cell 0 0,grow");
		
		tableScripts = new JTable();
		
		tableScripts.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int currentRow=tableScripts.getSelectedRow();
				textAreaScript.setText(theList.get(currentRow).getText());
			}
		});
		
		scrollPane.setViewportView(tableScripts);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Script", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, "cell 0 1,grow");
		panel.setLayout(new MigLayout("", "[410px,grow]", "[142px,grow]"));
		
		textAreaScript = new JTextArea();
		textAreaScript.setEditable(false);
		panel.add(textAreaScript, "cell 0 0,grow");
		
		fillData();
		
	}
}
