package com.enfocat.memoryLogo;

import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class MemBtn extends JButton {

	private String imatge;
	private Boolean match;
	private final String IMAGEPATH="img/";


	/**
	 * fa que botó NO mostri imatge
	 */
	public void ocultaImatge() {
		this.setIcon(null);
	}

	/**
	 * fa que botó mostri imatge
	 * potser poc eficient? (carrega icona cada cop...)
	 * millora: fer un array d'Image ja carregades a la classe...
	 */
	public void mostraImatge() {
		String imgFile = IMAGEPATH + this.getImatge();
		try {
			Image img = ImageIO.read(getClass().getResource(imgFile));
			this.setIcon(new ImageIcon(img));
		} catch (Exception ex) {
			System.out.println(ex);
		}

	}
	
	public boolean hasImage() {
		return (this.getIcon() != null);
	}

	
	
	/**
	 * CONSTRUCTOR principal
	 * el constructor normal de JButton posa a "text" el string que rep,
	 * en canvi aquest el considera imatge, i cridem el super sense arguments
	 * perquè no posi cap text al botó!
	 * @param img_file
	 */
	public MemBtn(String img_file) {
		super();
		this.setImatge(img_file);
		this.setMatch(false);
	}

	
	public MemBtn() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MemBtn(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public MemBtn(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	public MemBtn(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}


	public Boolean isMatch() {
		return match;
	}

	public void setMatch(Boolean match) {
		this.match = match;
	}

	public String getImatge() {
		return imatge;
	}

	public void setImatge(String imatge) {
		this.imatge = imatge;
	}

}
