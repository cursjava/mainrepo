package com.enfocat.memoryLogo;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class MemoryLogo extends JFrame {

	private MemBtn[] botons = new MemBtn[20];

	//indexos és la base per fer el shuffle
	private int[] indexos = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9};
	
	private MemBtn lastBtn = null;
	private JPanel contentPane;

	private final String[] IMATGES = {
			"cocacola.jpg","evian.jpg","kitkat.jpg","lipton.jpg","maggi.jpg",
			"milka.jpg","mms.jpg","oldelpaso.jpg","pepsi.jpg","pringles.jpg"};

	

	
	/**
	 * CONSTRUCTOR
	 */
	public MemoryLogo() {

		setTitle("Memory: Logos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 490, 660);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[110][110][110][110]", "[110][110][110][110][110][50]"));
	
		//barregem els indexos
		shuffleArray(indexos);
		
		// omplim l'array de botons passant imatge corresponent a index "barrejat"
		for (int i = 0; i < 20; i++) {
			botons[i] = new MemBtn(IMATGES[indexos[i]]);
		}
		

		// afegim els botons al contentPane i els assignem event click
		int i = 0;
		// X,Y  ens serveixen per ubicar el botó al grid del layout: "cell X  Y,grow"
		for (int y = 0; y < 5; y++) {
			for (int x = 0; x < 4; x++) {
				//afegim botó a content pane en la posició x y de MiGLayout
				contentPane.add(botons[i], "cell " + x + " " + y + ",grow");
				botons[i].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//obtenim l'objecte que dispara l'event, castejat com a MemBtn
						MemBtn j = (MemBtn) e.getSource();
						checkMemory(j); // li passem a checkMemory el botó clicat
					}
				});
				i++;
			}
		}

		// botó barreja
		JButton btnBarreja = new JButton("Barreja");
		contentPane.add(btnBarreja, "cell 0 5 2 1,grow");
		btnBarreja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				barreja();
			}
		});

		// botó mostra
		JButton btnSurt = new JButton("Surt");
		contentPane.add(btnSurt, "cell 2 5 2 1,grow");
		btnSurt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
	
	}



	/**
	 * check principal, després de cada click 
	 * @param btn
	 */
	private void checkMemory(MemBtn btn) {
		//si botó ja té imatge visible, no fem res: marxem
		if (btn.hasImage()) {
			return;
		}
		//si és el primer clicat, ocultem imatges del possible darrer intent
		if (lastBtn == null) {
			resetLastTry();
		}
		//en qualsevol cas fem que mostri imatge
		btn.mostraImatge();
		//si és el primer que cliquem...
		if (lastBtn == null) {
			//assignem last...
			lastBtn = btn;
		} else {
			//si és primer, mirem si match
			if (btn.getImatge().equals(lastBtn.getImatge())) {
				lastBtn.setMatch(true);
				btn.setMatch(true);
			} 
			lastBtn = null;
		}
	}

	

	/**
	 * resetLastTry amaga les imatges que no siguin match
	 */
	private void resetLastTry() {
		for (MemBtn btn : botons) {
			if (btn.hasImage() && !btn.isMatch()) {
				btn.ocultaImatge();
			}
		}
	}


	/**
	 * fa nou shuffle de l'array de números i reassigna tot
	 */
	private void barreja() {
		shuffleArray(indexos);
		for (int i = 0; i < 20; i++) {
			botons[i].ocultaImatge();
			botons[i].setImatge(IMATGES[indexos[i]]);
			botons[i].setMatch(false);
			lastBtn=null;
			}
	}
	
	
	// funció "pescada" a internet: simplement barreja de forma aleatòria un 
	// array de ints que li passem. com que el rep per referència, no cal reassignar
	// si no que el propi array ja queda barrejat.
	// 
	// Implementing Fisher–Yates shuffle
	private static void shuffleArray(int[] ar) {
		// If running on Java 6 or older, use `new Random()` on RHS here
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	
	
}
