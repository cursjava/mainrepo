package com.enfocat.memoryImg;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class MemoryImg extends JFrame {

	private JButton[] botons = new JButton[20];
	private int[] indexos = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9};
	
	private JButton lastBtn = null;
	private boolean modeMostra = false;
	private JButton btnMostra;
	private JPanel contentPane;
	
	private final String MATCH = "X";
	private final String EMPTY = "";
//	private final String[] SIMBOL = {"1","2","3","4","5","6","7","8","9","10"};
	
//	private final String[] SIMBOL = {"pera","poma","plàtan","kiwi","cirera","meló","síndria","tomata","taronja","pruna"};
//	private final String[] SIMBOL = {"A","B","C","D","E","F","G","H","I","J"};
//	private final String[] SIMBOL = {"SUN","MERCURY","VENUS","EARTH","MARS","JUPITER","SATURN","URANUS","NEPTUNE","PLUTO"};
	
	private final String[] SIMBOL = {"cocacola","evian","kitkat","lipton","maggi","milka","mms","oldelpaso","pepsi","pringles"};

	private final String[] BASEIMAGES = {"cocacola.jpg","evian.jpg","kitkat.jpg","lipton.jpg","maggi.jpg","milka.jpg",
			"mms.jpg","oldelpaso.jpg","pepsi.jpg","pringles.jpg"};
	private final String IMAGEPATH="img/";
	private String[] valors = new String[20];
	private String[] imatges = new String[20];
	
	
	
	// Implementing Fisher–Yates shuffle
	private static void shuffleArray(int[] ar) {
		// If running on Java 6 or older, use `new Random()` on RHS here
		Random rnd = ThreadLocalRandom.current();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			// Simple swap
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}

	/**
	 * reset posa a zero (EMPTY) tot el que no tingui un MATCH això "esborra"
	 * els valors d'una igualtat si no s'ha encertat.
	 * 
	 */
	private void reset() {
		int ct = 0;
		for (int i = 0; i < botons.length; i++) {
			JButton j = botons[i];
			if (j.getText() != MATCH) {
				j.setText(EMPTY);
				treuImatge(j);
			}
		}
	}

	private String valor(JButton btn){
		return valors[Integer.parseInt(btn.getName())];
	}
	private String img(JButton btn){
		return imatges[Integer.parseInt(btn.getName())];
	}

	
	
	
	/**
	 * similar a reset, però alterna entre posar a EMPTY i mostrar el número
	 * commuta botó
	 */
	private void mostra() {
		int ct = 0;
		for (int i = 0; i < botons.length; i++) {
			JButton j = botons[i];
			if (j.getText() != MATCH) {
				j.setText((modeMostra) ? EMPTY : valor(j));
				if (modeMostra) {
					treuImatge(j);
				}else{
					posaImatge(j);
				}
			}
		}
		modeMostra = !modeMostra;
		btnMostra.setText((modeMostra) ? "Oculta" : "Mostra");
	}

	/**
	 * fa nou shuffle de l'array de números i reassigna tot
	 * important! assignem Name, però deixem text a EMPTY
	 */
	private void barreja() {
		shuffleArray(indexos);
		for (int i = 0; i < 20; i++) {
			botons[i].setText(EMPTY);
			botons[i].setName(""+i); //name serà el mateix index
			valors[i]=SIMBOL[indexos[i]];
			imatges[i]=BASEIMAGES[indexos[i]];
				
			}
		modeMostra = false;
		btnMostra.setText("Mostra");

	}
	
	
	private void posaImatge(JButton btn){
		String imgFile=IMAGEPATH+img(btn);
		
		try {
		    Image img = ImageIO.read(getClass().getResource(imgFile));
		    btn.setIcon(new ImageIcon(img));
		  } catch (Exception ex) {
			  System.out.println(ex);
		  }
		
	}
	
	public void treuImatge(JButton btn){
		btn.setIcon(null);
	}

	/**
	 * 
	 * @param btn
	 * @throws InterruptedException
	 */
	private void checkMemory(JButton btn) {

		// els botons tenen un text "mostrat" i un name, que hem assignat a la
		// mateixa lletra
		// així podem esborrar i tornar a posar si cal el text "Mostrat"
		String textActual = btn.getText();
		String nom = valor(btn);

		if (lastBtn == null) {
			// si últim és null, repintem
			reset();
		}

		// establim el text del botó = al seu nom
		// amb això fa l'efecte que la lletra "apareix"
		btn.setText(nom);
		posaImatge(btn);

	
		//		  procés de verificació: 
		//		  1) si l'atribut lastBtn és null 
		//		  		a) li assignem el botó actual
		//		   	b) sortim, no fem res més 
		//		  2) si lastBtn NO és null, ja tenim un botó visible, cal mirar si coincideix amb actual 
		//		  		2.SI) èxit! els posem tots dos a "X" 
		//		  		2.NO) error, els posem tots dos a ""
		//		  En qualsevol dels dos casos, tornem a posar lastBtn a null


		if (lastBtn == null) {
			lastBtn = btn;
		} else {

			if (nom.equals(valor(lastBtn))) {
				// encert!
				lastBtn.setText(MATCH);
				btn.setText(MATCH);
			} 
			lastBtn = null;
		}
	}

	
	/**
	 * CONSTRUCTOR
	 */
	public MemoryImg() {
		//setup inicial del frame (automàtic)
		setTitle("Memory: \"old\" solar system");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 349, 256);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[150][150][150][150]", "[160][160][160][160][160][]"));
	
		//barregem els indeox
		shuffleArray(indexos);
		
		// Creem els botons i omplim l'array de botons
		for (int i = 0; i < 20; i++) {
			botons[i] = new JButton(EMPTY);
			botons[i].setName(""+i); //name serà el mateix index!
			valors[i]=SIMBOL[indexos[i]];
			imatges[i]=BASEIMAGES[indexos[i]];
			
			botons[i].setVerticalTextPosition(SwingConstants.BOTTOM);
			botons[i].setHorizontalTextPosition(SwingConstants.CENTER);
			    
//			posaImatge(botons[i]);
		}
		
		//primera barreja! per algun motiu falla
//		barreja();

		// afegim els botons al contentPane i els assignem event click
		int i = 0;
		// X,Y  ens serveixen per ubicar el botó al grid del layout: "cell X  Y,grow"
		for (int y = 0; y < 5; y++) {
			for (int x = 0; x < 4; x++) {
				//afegim botó a content pane
				contentPane.add(botons[i], "cell " + x + " " + y + ",grow");
				//li assignem listener x event click
				botons[i].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//obtenim l'objecte que dispara l'event
						JButton j = (JButton) e.getSource();
						//només actuem si el botó és buid, si cliquem sobre número o X no fem res
						if (j.getText() == EMPTY) {
							checkMemory(j); // li passem a checkMemory el botó clicat
						}
					}
				});
				i++;
			}
		}

		// botó barreja
		JButton btnBarreja = new JButton("Barreja");
		contentPane.add(btnBarreja, "cell 0 5 2 1,growx");
		btnBarreja.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				barreja();
			}
		});

		// botó mostra
		btnMostra = new JButton("Mostra");
		contentPane.add(btnMostra, "cell 2 5 2 1,growx");
		btnMostra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mostra();
			}
		});
	
	}

}
