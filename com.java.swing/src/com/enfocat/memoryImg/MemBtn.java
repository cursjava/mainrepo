package com.enfocat.memoryImg;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

public class MemBtn extends JButton{

	private String imatge;
	private Boolean match;
	private String valor;
	
	
	
	public MemBtn() {
		super();
		// TODO Auto-generated constructor stub
	}
	public MemBtn(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}
	public MemBtn(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}
	public MemBtn(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}
	public MemBtn(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}
	
	public Boolean getMatch() {
		return match;
	}
	public void setMatch(Boolean match) {
		this.match = match;
	}
	public String getImatge() {
		return imatge;
	}
	public void setImatge(String imatge) {
		this.imatge = imatge;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
}
