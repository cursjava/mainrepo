package com.enfocat.imgprueba;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Imagenes extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Imagenes frame = new Imagenes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Imagenes() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JButton btnNewButton = new JButton("New button");
		
		ponImagenBoton("img/evian.jpg", btnNewButton);

		contentPane.add(btnNewButton, "cell 0 0,grow");
	}
	
	public void ponImagenBoton(String imgPath, JButton btn){
		try {
		    Image img = ImageIO.read(getClass().getResource(imgPath));
		    btn.setIcon(new ImageIcon(img));
		  } catch (Exception ex) {
			  System.out.println("error cargando imagen...");
		  }
	}
	
	public void quitaImagenBoton(JButton btn){
		btn.setIcon(null);
	}
}
