package com.enfocat.inframes;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.enfocat.code.JInternalFrameScripts;

import net.miginfocom.swing.MigLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;

public class main extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPrueba = new JMenu("jmenu");
		menuBar.add(mnPrueba);
		
		JMenuItem mntmVentana = new JMenuItem("item 1");
		mntmVentana.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				internal f = new internal();
				f.setVisible(true);
				desktopPane.add(f);
			}
		});
		
		mnPrueba.add(mntmVentana);
		
		JMenu mnVentana = new JMenu("jmenu");
		mnPrueba.add(mnVentana);
		
		JMenuItem mntmSubventana = new JMenuItem("item3");
		mnVentana.add(mntmSubventana);
		
		JMenuItem mntmSubventana_1 = new JMenuItem("item4");
		mnVentana.add(mntmSubventana_1);
		
		JMenuItem mntmVentana_1 = new JMenuItem("item 2");
		mnPrueba.add(mntmVentana_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane);
	}
}
