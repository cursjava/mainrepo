package com.enfocat.DBConn;

import java.awt.EventQueue;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.enfocat.mysqltest.TiendaFrame01;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class MysqlDB {
	
	private static final String URL = "jdbc:mysql://192.168.10.10:3306/tienda";
	private static final String USERNAME = "ricard";
	private static final String PASSWORD = "1q2w3e4r";
	
	private static Connection conn;

	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TiendaFrame01 frame = new TiendaFrame01();
					frame.setVisible(true);
					
					CreateConnection();
					FillTable(frame.getThe_table(), "select * from tienda.catalogo limit 100");
					 conn.close();
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	
	private static void CreateConnection(){

		System.out.println("Connecting database...");

		try {
		
			conn=(Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);

		} catch (SQLException e) {
		    throw new IllegalStateException("Cannot connect the database!", e);
		}
	
	}
	

	public static void FillTable( JTable table, String Query)
	{
	    try
	    {
	       
	        Statement stat = (Statement) conn.createStatement();
	        ResultSet rs = stat.executeQuery(Query);
	        
	       

	        //To remove previously added rows
	        while( table.getRowCount() > 0) 
	        {
	            ((DefaultTableModel) table.getModel()).removeRow(0);
	        }
	        int columns = rs.getMetaData().getColumnCount();
	        while(rs.next())
	        {  
	            Object[] row = new Object[columns];
	            for (int i = 1; i <= columns; i++)
	            {  
	                row[i - 1] = rs.getObject(i);
	                
	            }
	            
	            System.out.println(row);
	            
	            //((DefaultTableModel) table.getModel()).insertRow(rs.getRow()-1,row);
	            ((DefaultTableModel) table.getModel()).addRow(row);
	        }

	        rs.close();
	        stat.close();
	       
	    }
	    catch( SQLException e)
	    {
	    	e.printStackTrace();
	    }
	}
	
	


	public static void justDoIt(Connection dbconex)
		    throws SQLException {

		    Statement stmt = null;
		    String query = "select * from tienda.catalogo limit 100";
		    try {
		        stmt = (Statement) dbconex.createStatement();
		        ResultSet rs = stmt.executeQuery(query);
		        while (rs.next()) {
		            String name = rs.getString("producto");
		            String composicion = rs.getString("composicion");
			            String fab = rs.getString("fabricante");
		            int id = rs.getInt("id");
		            System.out.println(id + "\t" + fab + "\t" + name +"\t"+composicion);
		        }
		    } catch (SQLException e ) {
//		        JDBCTutorialUtilities.printSQLException(e);
		        e.printStackTrace();
		    } finally {
		        if (stmt != null) { stmt.close(); }
		    }
		}


	public static DefaultTableModel buildTableModel(ResultSet rs)
	        throws SQLException {

	    ResultSetMetaData metaData = rs.getMetaData();

	    // names of columns
	    Vector<String> columnNames = new Vector<String>();
	    int columnCount = metaData.getColumnCount();
	    for (int column = 1; column <= columnCount; column++) {
	        columnNames.add(metaData.getColumnName(column));
	    }

	    // data of the table
	    Vector<Vector<Object>> data = new Vector<Vector<Object>>();
	    while (rs.next()) {
	        Vector<Object> vector = new Vector<Object>();
	        for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
	            vector.add(rs.getObject(columnIndex));
	        }
	        data.add(vector);
	    }

	    return new DefaultTableModel(data, columnNames);

	}
}
