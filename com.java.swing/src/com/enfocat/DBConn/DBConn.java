package com.enfocat.DBConn;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.enfocat.mysqltest.GUI;
import com.mysql.jdbc.Connection;
import com.enfocat.codeEntities.*;
import java.util.*;

public class DBConn {

	private static final String URL = "jdbc:mysql://192.168.10.10:3306/code";
	private static final String USERNAME = "ricard";
	private static final String PASSWORD = "1q2w3e4r";
	private static final Logger LOG = Logger.getLogger(GUI.class.getName());

	
	public static List<Script> loadScripts() {

		LOG.info("START loading scripts");
		List<Script> scriptsList = new ArrayList<Script>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select id,name,description,code, category_id from code.scripts");

			while (rs.next()) {
				scriptsList.add(new Script(
						(Long) rs.getObject(1),
						(String) rs.getObject(2),
						(String) rs.getObject(3),
						(String) rs.getObject(4),
						(Long) rs.getObject(5)
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Exception in Load Data", e);
		}

		LOG.info("END loadData method");
		return scriptsList;

	}

	
	public static List<Script> loadScriptsCat(int cat_id) {

		LOG.info("START loading scripts");
		List<Script> scriptsList = new ArrayList<Script>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select id,name,description,code, category_id from code.scripts where category_id="+(cat_id+1));

			while (rs.next()) {
				scriptsList.add(new Script(
						(Long) rs.getObject(1),
						(String) rs.getObject(2),
						(String) rs.getObject(3),
						(String) rs.getObject(4),
						(Long) rs.getObject(5)
						));
			}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Exception in Load Data", e);
		}

		LOG.info("END loadData method");
		return scriptsList;

	}
	
	public static List<Category> loadCats() {

		LOG.info("START loading scripts");
		List<Category> catsList = new ArrayList<Category>();

		try (Connection conn = (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery("select id,name from code.cats");

			while (rs.next()) {
				catsList.add(new Category(
						(Long) rs.getObject(1),
						(String) rs.getObject(2)
						));
				}

		} catch (Exception e) {
			LOG.log(Level.SEVERE, "Exception in Load Data", e);
		}

		LOG.info("END loadData method");
		return catsList;

	}
	
	

}
