package com.enfocat.databind;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JDesktopPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFrameMain extends JFrame {

	private JPanel contentPane;
	private JDesktopPane desktopPaneMain;

	/**
	 * https://www.youtube.com/watch?v=cuueXSz2pB0
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrameMain frame = new JFrameMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFrameMain() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnDemo = new JMenu("Demo");
		menuBar.add(mnDemo);
		
		JMenuItem mntmCombobox = new JMenuItem("Combobox");
		mntmCombobox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JInternalFrameComboBox f = new JInternalFrameComboBox();
				f.setVisible(true);
				desktopPaneMain.add(f);
			}
		});
		mnDemo.add(mntmCombobox);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPaneMain = new JDesktopPane();
		contentPane.add(desktopPaneMain, BorderLayout.CENTER);
	}
}
