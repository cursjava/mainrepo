package com.enfocat.databind;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JInternalFrameComboBox extends JInternalFrame {
	private JComboBox comboBoxSimple;
	private JLabel lblContent;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JInternalFrameComboBox frame = new JInternalFrameComboBox();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	private void FillData(){
		DefaultComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<String>();
		defaultComboBoxModel.addElement("Item1");
		defaultComboBoxModel.addElement("Item2");
		defaultComboBoxModel.addElement("Item3");
		defaultComboBoxModel.addElement("Item4");
		comboBoxSimple.setModel(defaultComboBoxModel);
	}
	
	
	/**
	 * Create the frame.
	 */
	public JInternalFrameComboBox() {
		setTitle("Fill data to ComboBox");
		setClosable(true);
		setMaximizable(true);
		setResizable(true);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);
		
		JLabel lblSimple = new JLabel("Simple");
		lblSimple.setBounds(49, 27, 57, 15);
		getContentPane().add(lblSimple);
		
		comboBoxSimple = new JComboBox();
		comboBoxSimple.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblContent.setText(String.valueOf(comboBoxSimple.getSelectedIndex()));
			}
		});
		comboBoxSimple.setBounds(107, 22, 215, 25);
		getContentPane().add(comboBoxSimple);
		
		lblContent = new JLabel("");
		lblContent.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblContent.setBounds(107, 73, 206, 25);
		getContentPane().add(lblContent);
		
		FillData();

	}
}
