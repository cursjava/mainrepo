package com.enfocat.images;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageFrame extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImageFrame frame = new ImageFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ImageFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
		
		JButton btnNewButton = new JButton("");
//		btnNewButton.setIcon(new ImageIcon(ImageFrame.class.getResource("/com/sun/java/swing/plaf/windows/icons/JavaCup32.png")));
		
		try {
		    Image img = ImageIO.read(getClass().getResource("images/transalp.jpg"));
		    btnNewButton.setIcon(new ImageIcon(img));
		  } catch (Exception ex) {
			  System.out.println("error...");
		    System.out.println(ex);
		  }
		
		contentPane.add(btnNewButton, "cell 0 0,grow");
	}

}
