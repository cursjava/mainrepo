package com.enfocat.mysqltest;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JTable;

public class TiendaFrame01 extends JFrame {

	private JPanel contentPane;
	private JTable the_table;
	private JTable table;



	/**
	 * Create the frame.
	 */
	public TiendaFrame01() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 558, 465);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[][grow]"));
		
		JButton btnNewButton = new JButton("New button");
		contentPane.add(btnNewButton, "cell 0 0");
		
		the_table = new JTable();
		contentPane.add(the_table, "flowy,cell 0 1,grow");
		
		table = new JTable();
		contentPane.add(table, "cell 0 1");
	}



	public JTable getThe_table() {
		return the_table;
	}



}
