package com.enfocat.codeEntities;

public class Script {

	private Long id;
	private String name;
	private String desc;
	private String text;
	private Long category_id;

	public Script(Long id, String name, String desc, String text, Long category_id) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.text = text;
		this.category_id = category_id;
		
	}

	
	public Script() {
		super();
	}

	@Override
	public String toString() {
		return "[" + id + "] " + name;
	}

	public Long getCategory_id() {
		return category_id;
	}


	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
